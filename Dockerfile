FROM gradle:6.9.0-jdk11-openj9 AS product-service
RUN mkdir -p /app/code
COPY src /app/code/src
ADD build.gradle /app/code/build.gradle
WORKDIR /app/code
RUN gradle clean build -x test

#New Layer
FROM openjdk:11-jre
RUN mkdir -p /app/output
COPY --from=product-service /app/code/build/libs/code-0.0.1-SNAPSHOT.jar /app/output/code-0.0.1-SNAPSHOT.jar
EXPOSE 9001
ENTRYPOINT ["java","-jar","/app/output/code-0.0.1-SNAPSHOT.jar"]
