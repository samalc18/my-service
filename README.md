# Product-Service

# Steps to Complie & Create Image

From root directory execute ./gradlew clean build -x test

# Setup Database

docker run -d -p 3306:3306 -e MYSQL_USER=productadmin -e MYSQL_PASSWORD=productadmin -e MYSQL_ROOT_PASSWORD=productadmin  -e MYSQL_DATABASE=product-service --name product-db mysql:5.5

# Run Application

After build is successful go to

build/libs folder and execute

"java -jar *.jar

# Access Application

http://localhost:9001/swagger-ui.html

